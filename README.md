# Simple CRUD App
## Installation
Use the npm menager to install the dependencies: 
```bash
npm install
```
## Usage
Go to the server directory and use the below command to start the backend:
```bash
npm start
```
Go to the client directory and use the below command to start the frontend:
```bash
npm start
```
That's it. You can create, read, update and delete posts from the Mongo database.